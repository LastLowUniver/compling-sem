window.onload = async () => {
    await go()
}

async function go() {
    console.log('1');
    const res = await fetch("https://vue-with-http-499c5-default-rtdb.firebaseio.com/news_publish.json")
    const data = await res.json()

    for (let item in data) {
        document.querySelector('tbody').innerHTML += `
        <tr>
            <th scope="row">${data[item].url.split('/').at(-1)}</th>
            <td>${data[item].date}</td>
            <td>${data[item].header}</td>
            <td class="overflow-hidden" >
                <div style="white-space: nowrap; width: 300px; display: inline-block">${data[item].text}</div>
            </td>
            <td>${data[item].countReview}</td>
            <td>${data[item].url}</td>
        </tr>
        `
    }
}