window.onload = async () => {
    await go()
}

async function go() {
    console.log('1');
    const res = await fetch("https://vue-with-http-499c5-default-rtdb.firebaseio.com/sentenses_tonation_publish/-NLxGyEoIKL-mnVAie4k.json")
    const data = await res.json()

    for (let name in data) {
        for (let year in data[name]) {
            for (let elem in data[name][year]) {
                document.querySelector('tbody').innerHTML += `
                <tr>
                    <th scope="row">${data[name][year][elem].url.split('/').at(-1)}</th>
                    <td>${name}</td>
                    <td>${year}</td>
                    <td class="overflow-hidden" >
                        <div style="white-space: nowrap; width: 300px; display: inline-block">${data[name][year][elem].sentence}</div>
                    </td>
                    <td style="${data[name][year][elem].rate === 'Positive' ? 'color: green;' : 'color: red;'}">${data[name][year][elem].rate}</td>
                    <td>
                        <div style="white-space: nowrap; width: 80px; display: inline-block">${data[name][year][elem].url}</div>
                    </td>
                </tr>
                `
            }
        }
    }
}