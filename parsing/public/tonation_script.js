window.onload = async () => {
    await go()
}

async function go() {
    console.log('1');
    const res = await fetch("https://vue-with-http-499c5-default-rtdb.firebaseio.com/sentences_rating_init/-NLwzWarnS1np0esiu9O.json")
    const data = await res.json()

    for (let item in data) {
        document.querySelector('tbody').innerHTML += `
        <tr>
            <th scope="row">${item}</th>
            <td>${data[item]["2020"] ?? "Нет упоминаний"}</td>
            <td>${data[item]["2021"] ?? "Нет упоминаний"}</td>
            <td>${data[item]["2022"] ?? "Нет упоминаний"}</td>
        </tr>
        `
    }
}