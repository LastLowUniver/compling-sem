import axios from "axios";

const getDataInOneNews = async (id, url) => {
    try {
        const response = await axios(
            `https://s02.api.yc.kpcdn.net/content/api/1/pages/get.json?callback=cb-5578667&pages.direction=current&pages.target.class=10&pages.target.id=${id}&sub=1`
        )

        const arr = await response.data

        let result = {}

        result.text = ""
        for (let item of arr["cb-5578667"]["childs"][0]["childs"]) {
            if (item['@context'] == "paragraph") {
                result.text += item["ru"]["text"]
            }
        }

        result.url = url
        result.date = arr["cb-5578667"]["datePublished"]
        result.header = arr["cb-5578667"]["headline"]
        result.countReview = arr["cb-5578667"]["meta"][4]["value"]

        await axios.post('https://vue-with-http-499c5-default-rtdb.firebaseio.com/news_publish.json', result)
    } catch (error) {
        console.log(error.message);
    }
}


const getDataInMonthAndYear = async (number, month, year) => {
    try {
        const { data } = await axios(
            `https://s02.api.yc.kpcdn.net/content/api/1/pages/get.json?pages.age.month=${month}&pages.age.year=${year}&pages.direction=page&pages.number=${number}&pages.target.class=100&pages.target.id=5`
        )

        for (let item of data.childs) {
            await getDataInOneNews(item["@id"], `https://www.volgograd.kp.ru/${data["@type"]}/${item["@type"][0]}/${item["@id"]}`)
        }
    } catch (error) {
        console.log(error.message);
    }
}


async function __main__() {
    for (let i = 12; i > 0; i--) {
        for (let j = 30; j > 0; j--) {
            await getDataInMonthAndYear(j, i, 2022)
            console.log(i, j);
        }
    }
}


__main__()




