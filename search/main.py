from natasha import (
    Segmenter,
    MorphVocab,

    NewsEmbedding,
    NewsMorphTagger,
    NewsSyntaxParser,
    NewsNERTagger,

    PER,
    NamesExtractor,
    DatesExtractor,

    Doc
)
import requests
# from fuzzywuzzy import fuzz

response_1 = requests.get('https://vue-with-http-499c5-default-rtdb.firebaseio.com/places.json').json()

response_persons = requests.get('https://vue-with-http-499c5-default-rtdb.firebaseio.com/persons.json').json()

res_places = response_1["-NLi5Spnik1j1qpu1lC9"] + response_persons["-NLgVzOyOLzul6lZ4_ta"]


places = []

# for item in response_1:
for elem in res_places:
    list = [_ for _ in elem.replace(".", "").replace("-", " ").replace("\"", "").split(" ") if len(_) > 2]

    for i in range(len(list)):
        if "я" == list[i][-1:] or "го" == list[-2:]:
            list[i] = list[i][:-2]
        else:
            list[i] = list[i][:-1]

    places.append(list)



result = {}

for item in res_places:
    result[item] = {
        "2022": [],
        "2021": [],
        "2020": []
    }


try:
    segmenter = Segmenter()
    morph_vocab = MorphVocab()

    emb = NewsEmbedding()
    morph_tagger = NewsMorphTagger(emb)
    syntax_parser = NewsSyntaxParser(emb)
    ner_tagger = NewsNERTagger(emb)

    names_extractor = NamesExtractor(morph_vocab)
    dates_extractor = DatesExtractor(morph_vocab)

    # --------------------------------------------------------

    response = requests.get(
        'https://vue-with-http-499c5-default-rtdb.firebaseio.com/news.json').json()

    print(len(response))

    count_length = 0
    for item in response:
        text = response[item]['text']
    # text = "Домашние матчи «Ротора» в первой части сезона 2022/2023 годов стали одним из самых посещаемых среди всех команд России. По зрительной аудитории игры на «Волгоград Арене» вошли в топ-5, об этом сообщает сайт volgograd.kp.ru со ссылкой на телеграм-канал «Футбольная посещаемость». В конце лета и осенью «Ротор» во Второй лиге провел 9 домашних матчей, их средняя посещаемость составила 13 114 человек. «Волгоград Арена» в среднем заполнялась на 28,8%."
        doc = Doc(text)
        doc.segment(segmenter)
        doc.tag_morph(morph_tagger)
        doc.parse_syntax(syntax_parser)
        doc.tag_ner(ner_tagger)

        check = False

        for sent in doc.sents:
            index_places = 0
            for items_places in places:
                count = 0
                for word_place in items_places:
                    if word_place.lower() in sent.text.lower():
                        count+=1
                    if count > 1:
                        print(sent.text)
                        result[res_places[index_places]][response[item]['date'].split('-')[0]].append({
                            "sentence": sent.text,
                            "id": str(item),
                            "url": response[item]['url']
                        })
                        count_length+=1
                        check = True
                        break
                if check == True:
                    break

                index_places+=1
            if check == True:
                break

        
    print("\n" + str(count_length) + "\n")
    requests.post(url="https://vue-with-http-499c5-default-rtdb.firebaseio.com/sentenses_parsion_init.json", json=result)

    

 
except KeyboardInterrupt:
    print()





#     for span in doc.spans:
#         span.normalize(morph_vocab)

#     for span in doc.spans:
#         span.extract_fact(names_extractor)

#     names_dist = {_.normal: _.fact.as_dict for _ in doc.spans if _.fact}

# for name in names_dist.keys():
# for item in persons:
#     if fuzz.ratio(item, name) > 80:
#         requests.post('https://vue-with-http-499c5-default-rtdb.firebaseio.com/news_info.json', item)


