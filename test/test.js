import axios from "axios";
import { load } from "cheerio";

// (async () => {
//     let resultArr = []

//     for (let i = 0; i < 61; i += 20) {
//         const { data } = await axios("https://global-volgograd.ru/person?offset=" + i)

//         let data1 = data.replace(/\n/g, '')
//         data1 = data1.replace(/\r/g, '')

//         let arr = data1.match(/<div class="person-text">\s*<div class="title">\s*<a href="https:\/\/global-volgograd.ru\/person\/id\/[0-9]*">(\s?[а-яА-ЯЁё]+\s[а-яА-ЯЁё]+\s?[а-яА-ЯЁё]*)/g)
//         for (let item of arr) {
//             resultArr.push(item.split('>').at(-1).trim())
//         }
//     }

// await axios.post("https://vue-with-http-499c5-default-rtdb.firebaseio.com/persons.json", resultArr);
// })()

(async () => {
    let resultArr = []

    const { data } = await axios("https://avolgograd.com/sights?obl=vgg")

    let $ = load(data)

    $('.ta-211 a').each(function () {
        resultArr.push($(this).text())
    });

    resultArr = [
        'Волгоград Арена',
        'Старая Сарепта',
        'Волгоградский музей им И И Машкова',
        'Казанский кафедральный собор',
        'Волгоградская филармония',
        'Научная библиотека им М Горького',
        'Площадь Павших Борцов',
        'Памятник Саше Филиппову',
        'Памятник чекистам',
        'Армянская церковь Святого Георгия',
        'Трамвай-памятник',
        'Воинский эшелон',
        'Мельница Гергардта',
        'Памятник Дзержинскому',
        'Здание Царицынской пожарной команды',
        'Домп Павлова',
        'Челябинский колхозник',
        'Памятник Гоголю',
        'Бейт Давид',
        'Лысая гора',
        'Памятник Паникахе',
        'Фонтан влюбленных',
        'Музей-квартира Луконина М К'
    ]

    await axios.post("https://vue-with-http-499c5-default-rtdb.firebaseio.com/places.json", resultArr);

})()