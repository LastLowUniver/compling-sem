#!flask/bin/python
from flask import Flask, jsonify, request, render_template
from flask_cors import CORS, cross_origin
from gensim.models import Word2Vec

app = Flask(__name__, template_folder="client")

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

model = Word2Vec.load('./word2vec_model/word2vec.model')


# route for getting datd lab 6
@app.route('/word', methods=['GET'])
@cross_origin()
def word2Vec():
    list = model.wv.most_similar(positive=[request.args.get('word')])
    return jsonify({'result': list})



if __name__ == '__main__':
    app.run(debug=True)
